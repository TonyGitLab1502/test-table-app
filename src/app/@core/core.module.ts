import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { InterceptorService } from "@core/services/interceptor.service";
import { BackendModule } from "@core/backend/backend.module";


@NgModule({
  imports: [
    BackendModule,
    CommonModule
  ],
  exports: [BackendModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ]
})
export class CoreModule { }
