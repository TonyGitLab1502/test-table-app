import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpStatusCode
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { mockData as data } from '@assets/mock-data';
import { IEvent } from '@backend/events/event.interface';

import { find as _find, flow as _flow, map as _map, max as _max, reject as _reject } from 'lodash/fp';


@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  private readonly STORAGE_KEY = 'mockInfo';
  public events!: IEvent[];

  constructor() {
    const savedData: string | null = localStorage.getItem(this.STORAGE_KEY);
    this.events = savedData ? JSON.parse(savedData) : data;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    switch(req.method.toUpperCase()) {
      case 'POST':
        return this.addEvent(req);
      case 'PUT':
        return this.updateEvent(req);
      case 'DELETE':
        return this.removeEvent(req);
      default:
        return this.getEvents();
    }
  }

  public getEvents(): Observable<HttpEvent<any>> {
    return this.success(this.events);
  }

  public addEvent(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    const { body } = req;
    const maxEventIdValue = _flow(_map('eventId'), _max)(this.events);

    const newEvent = body;
    newEvent.eventId = maxEventIdValue + 1;

    this.events.push(newEvent);
    this.updateEventsInStorage(this.events);

    return this.success(newEvent);
  }

  public updateEvent(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    const { url, body } = req;
    const id = this.getIdFromUrl(url);
    const foundInfoItem = _find<IEvent>({ eventId: id})(this.events);

    if (!foundInfoItem) {
      return this.error(`Record with eventId ${id} not found`);
    }

    Object.assign(foundInfoItem, body);
    this.updateEventsInStorage(this.events);

    return this.success();
  }

  public removeEvent(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    this.events = _reject<IEvent>({ eventId: this.getIdFromUrl(req.url) })(this.events);
    this.updateEventsInStorage(this.events);

    return this.success();
  }

  private success(body?: any): Observable<HttpResponse<any>> {
    return of(new HttpResponse({status: HttpStatusCode.Ok, body}));
  }

  private error(message: string): Observable<never> {
    return throwError({ error: message });
  }

  private updateEventsInStorage(events: IEvent[]): void {
    window.localStorage.setItem(this.STORAGE_KEY, JSON.stringify(events))
  }

  private getIdFromUrl(url: string): number {
    const urlParts = url.split('/');

    return parseInt(urlParts[urlParts.length - 1]);
  }
}
