export interface IEvent {
  type: string;
  cowId: number;
  animalId: string;
  eventId: number;
  deletable: boolean;
  lactationNumber: number;
  daysInLactation: number;
  ageInDays: number;
  startDateTime: number;
  reportingDateTime: number;

  healthIndex: number | null;
  endDate: number | null;
  minValueDateTime: number | null;
  alertType: string | null;
  duration: number | null;
  originalStartDateTime: number | null;
  endDateTime: number | null;
  daysInPregnancy: number | null;
  heatIndexPeak: string | null;
  newGroupId: number | null;
  newGroupName: string | null;
  currentGroupId: number | null;
  currentGroupName: string | null;
  sire: string | null;
  breedingNumber: number | null;
  isOutOfBreedingWindow: boolean;
  interval: number | null;
  destinationGroup: number | null;
  destinationGroupName: string | null;
  cowEntryStatus: string | null;
  calvingEase: string | null;
  oldLactationNumber: number | null;
  newborns: string | null;
  birthDateCalculated: boolean;
}

export interface IEventExtend extends IEvent {
  checked: boolean;
  isEditMode: boolean;
}
