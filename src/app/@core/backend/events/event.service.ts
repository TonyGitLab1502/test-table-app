import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IEvent } from '@backend/events/event.interface';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  constructor(
    private readonly http: HttpClient
  ) { }

  public getEvents(): Observable<Partial<IEvent>[]> {
    return this.http.get<Partial<IEvent>[]>('/event');
  }

  public createEvent(event: IEvent): Observable<Partial<IEvent>> {
    return this.http.post<Partial<IEvent>>('/event', event);
  }

  public updateEvent(id: number, event: IEvent): Observable<any> {
    return this.http.put(`/event/${id}`, event);
  }

  public removeEvent(id: number): Observable<any> {
    return this.http.delete(`/event/${id}`);
  }
}
