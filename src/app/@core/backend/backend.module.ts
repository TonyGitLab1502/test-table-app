import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventService } from '@backend/events/event.service';



@NgModule({
  imports: [CommonModule],
  providers: [EventService]
})
export class BackendModule { }
