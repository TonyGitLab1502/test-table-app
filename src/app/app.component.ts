import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppService } from './app.service';
import { IEventExtend } from '@backend/events/event.interface';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import {
  flow as _flow,
  forEach as _forEach,
  includes as _includes,
  map as _map,
  reject as _reject,
  uniq as _uniq
} from 'lodash/fp';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public columns: string[] = [
    'action', 'healthIndex', 'endDate', 'minValueDateTime', 'type', 'cowId', 'animalId', 'eventId', 'deletable', 'lactationNumber',
    'daysInLactation', 'ageInDays', 'startDateTime', 'reportingDateTime', 'alertType', 'duration', 'originalStartDateTime',
    'endDateTime', 'daysInPregnancy', 'heatIndexPeak', 'newGroupId', 'newGroupName', 'currentGroupId', 'currentGroupName',
    'sire', 'breedingNumber', 'isOutOfBreedingWindow', 'interval', 'destinationGroup', 'destinationGroupName', 'cowEntryStatus',
    'calvingEase', 'oldLactationNumber', 'newborns', 'birthDateCalculated'
  ];

  public columnsWithIds: string[] = ['cowId', 'animalId', 'eventId', 'newGroupId', 'currentGroupId'];
  public cachedRow: IEventExtend | null = null;
  public selectedRows: IEventExtend[] = [];
  public events$: BehaviorSubject<IEventExtend[]> = new BehaviorSubject<IEventExtend[]>([]);


  constructor(
    public readonly base: AppService,
    private readonly snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.base.loadEvents().subscribe((events: IEventExtend[]) => this.events$.next(events));
  }

  ngOnDestroy(): void {
    this.events$.unsubscribe();
  }

  public addData(): void {
    const randomItemIdx = Math.floor(Math.random() * this.events$.value.length);

    this.base.addEvent(this.events$.value[randomItemIdx])
      .pipe(
        tap((events: IEventExtend[]) => {
          this.events$.next([events[0], ...this.events$.value]);
          this.snackBar.open(`New item with eventId ${ events[0].eventId } successfully added`, 'Ok');
        })
      )
      .subscribe();
  }

  public updateData(row: IEventExtend): void {
    row.isEditMode = false;

    this.base.updateEvent(row)
      .pipe(
        tap(() => {
          this.cachedRow = null;
          this.snackBar.open(`Item with eventId ${row.eventId} successfully updated`, 'Ok');
        })
      )
      .subscribe()
  }

  public removeData(): void {
    const ids = _flow(_map((row: IEventExtend) => row.eventId), _uniq)(this.selectedRows);

    this.base.removeEvents(ids)
      .pipe(
        tap(() => {
          this.events$.next(_reject((event: IEventExtend) => _includes(event.eventId)(ids))(this.events$.value));
          this.updateCheckedStatus(this.events$.value);
          this.selectedRows = [];

          this.snackBar.open(
            `Item${ids.length > 1 ? 's': ''} with eventId${ids.length > 1 ? 's': ''} ${ids.join(', ')} successfully removed`,
            'Ok'
          );
        })
      )
      .subscribe();
  }

  public toggleRow(value: MatCheckboxChange, row: IEventExtend): void {
    const { checked } = value;

    this.updateCheckedStatus([row], checked);

    if (checked) {
      this.selectedRows.push(row);
    } else {
      this.selectedRows = _reject<IEventExtend>({eventId: row.eventId})(this.selectedRows);
    }
  }

  private updateCheckedStatus(events: IEventExtend[], checked: boolean = false): void {
    _forEach((event: IEventExtend) => event.checked = checked)(events);
  }

  public toggleRowEditMode(row: IEventExtend, rowIdx: number): void {
    row.isEditMode = !row.isEditMode;

    if (row.isEditMode) {
      this.cachedRow = {...row};
    } else {
      if (this.cachedRow) {
        const items: IEventExtend[] = this.events$.value;

        this.cachedRow.isEditMode = false;
        items[rowIdx] = this.cachedRow;
        this.events$.next([...items]);
        this.cachedRow = null;
      }
    }
  }
}
