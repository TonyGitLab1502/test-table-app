import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { IEvent, IEventExtend } from '@backend/events/event.interface';
import { EventService } from '@backend/events/event.service';
import { map, mapTo } from 'rxjs/operators';
import { map as _map } from 'lodash/fp';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(
    private readonly infoService: EventService
  ) { }

  public loadEvents(): Observable<IEventExtend[]> {
    return this.infoService.getEvents().pipe(
      map((data: Partial<IEvent>[]) => this.mapEvents(data))
    );
  }

  public addEvent(event: IEventExtend): Observable<IEventExtend[]> {
    const { checked, isEditMode, ...result } = event;

    return this.infoService.createEvent(result)
      .pipe(
        map(($event: Partial<IEvent>) => this.mapEvents([$event]))
      );
  }

  public updateEvent(event: IEventExtend): Observable<boolean> {
    const { checked, isEditMode, ...result } = event;

    return this.infoService.updateEvent(result.eventId, result).pipe(mapTo(true));
  }

  public removeEvents(ids: number[]): Observable<boolean> {
    const collection: Observable<any>[] = _map((id: number) => this.infoService.removeEvent(id))(ids);

    return forkJoin(collection).pipe(mapTo(true));
  }

  private mapEvents(items: Partial<IEvent>[]): IEventExtend[] {
    return _map((item: Partial<IEvent>) => {
      return Object.assign({
        type: null,
        cowId: null,
        animalId: null,
        eventId: null,
        deletable: false,
        lactationNumber: null,
        daysInLactation: null,
        ageInDays: null,
        startDateTime: null,
        reportingDateTime: null,
        healthIndex: null,
        endDate: null,
        minValueDateTime: null,
        alertType: null,
        duration: null,
        originalStartDateTime: null,
        endDateTime:  null,
        daysInPregnancy: null,
        heatIndexPeak: null,
        newGroupId: null,
        newGroupName: null,
        currentGroupId: null,
        currentGroupName: null,
        sire: null,
        breedingNumber: null,
        isOutOfBreedingWindow: false,
        interval: null,
        destinationGroup: null,
        destinationGroupName: null,
        cowEntryStatus: null,
        calvingEase: null,
        oldLactationNumber: null,
        newborns: null,
        birthDateCalculated: false,
        checked: false,
        isEditMode: false
      }, item);
    })(items);
  }
}
